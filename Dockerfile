FROM python:3.8-slim-buster
#FROM dash-vis:python
WORKDIR /app
COPY . .
RUN pip3 install -r requirements.txt
CMD ["python","/app/Dash-Vis.py"]
